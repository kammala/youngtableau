#include <iostream>
#include <young.h>

#include <algorithm>

using namespace std;

int main (int argc, char ** argv)
{
    cout << "Start working..." << endl;

    YoungTableau<int> yt(4, 4);
    infinitype<int> x(13);
    yt.insert(11);
    yt.insert(10);
    yt.insert(9);

    yt.insert(1);
    yt.insert(2);
    yt.insert(3);
    yt.insert(4);

    yt.insert(6);
    yt.insert(5);
    yt.insert(7);
    yt.insert(8);

    yt.insert(16);
    yt.insert(14);
    yt.insert(12);
    cout << "empty: " << (yt.isEmpty() ? "yes" : "no") << endl << "full: " << (yt.isFull() ? "yes" : "no") << endl;
    cout << "Original tableau:" << endl << yt.getPrintableTableau() << endl;

    pair<int, int> result = yt.search(12);
    cout << 12 << ": " << result.first << " " << result.second << endl;
    result = yt.search(8);
    cout << 8 << ": " << result.first << " " << result.second << endl;
    result = yt.search(3);
    cout << 3 << ": " << result.first << " " << result.second << endl;
    cout << "End working..." << endl;
    return 0;
}
