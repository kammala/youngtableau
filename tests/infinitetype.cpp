#include <gtest/gtest.h>
#include <young.h>

TEST(FloatingInfiniteTypeComparision, Finite)
{
    infinitype<double> x(0), y(1.2), z(1.2);
    EXPECT_TRUE(y > x);
    EXPECT_TRUE(x < y);
    EXPECT_TRUE(y == z);
}

TEST(FloatingInfiniteTypeComparision, Infinite)
{
    infinitype<double> x(0), y(1.2),
            z = infinitype<double>::getInf(),
            w = infinitype<double>::getInf(),
            v = -z;
    EXPECT_TRUE(z.isPositiveInf());
    EXPECT_TRUE(v.isNegativeInf());
    EXPECT_TRUE(x < z);
    EXPECT_TRUE(y < z);
    EXPECT_FALSE(z == z);

    EXPECT_TRUE(x > v);
    EXPECT_TRUE(z > v);
    EXPECT_FALSE(z > w);
    EXPECT_FALSE(z < w);
    EXPECT_FALSE(w > z);
    EXPECT_FALSE(w < z);
    EXPECT_FALSE(z == w);
}


TEST(IntegerInfiniteTypeComparision, Infinite)
{
    infinitype<int> x(-1), y(1),
            z = infinitype<int>::getInf(),
            w = infinitype<int>::getInf(),
            v = -z;
    EXPECT_TRUE(z.isPositiveInf());
    EXPECT_TRUE(v.isNegativeInf());
    EXPECT_TRUE(x < z);
    EXPECT_TRUE(y < z);
    EXPECT_FALSE(z == z);

    EXPECT_TRUE(x > v);
    EXPECT_TRUE(z > v);
    EXPECT_FALSE(z > w);
    EXPECT_FALSE(z < w);
    EXPECT_FALSE(w > z);
    EXPECT_FALSE(w < z);
    EXPECT_FALSE(z == w);
}
