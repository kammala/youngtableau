#include <gtest/gtest.h>
#include <young.h>

class YoungTableauFixture: public ::testing::Test
{
  protected:
    YoungTableauFixture(): usual(YoungTableau<int>(4, 4)),
        empty(YoungTableau<int>(4, 4)),
        fully(YoungTableau<int>(4, 4)),
        rect(YoungTableau<int>(2, 4))
    {
        usual.insert(12); usual.insert(11); usual.insert(10); usual.insert(9);
        usual.insert(1); usual.insert(2); usual.insert(3); usual.insert(4);
        usual.insert(6); usual.insert(5); usual.insert(7); usual.insert(8);
        usual.insert(16); usual.insert(14);

        fully.insert(14); fully.insert(11); fully.insert(10); fully.insert(6);
        fully.insert(1); fully.insert(7); fully.insert(2); fully.insert(4);
        fully.insert(9); fully.insert(12); fully.insert(3); fully.insert(8);
        fully.insert(16); fully.insert(5); fully.insert(15); fully.insert(13);

        rect.insert(4); rect.insert(6); rect.insert(1); rect.insert(5);
        rect.insert(2); rect.insert(3);
    }

    virtual void SetUp()
    {
        std::cout << "Usual case: " << usual.getPrintableTableau() << std::endl;
        std::cout << "Fully case: " << fully.getPrintableTableau() << std::endl;
        std::cout << "Rect case: " << rect.getPrintableTableau() << std::endl;
    }

    YoungTableau<int> usual;
    YoungTableau<int> empty;
    YoungTableau<int> fully;
    YoungTableau<int> rect;
};

TEST_F(YoungTableauFixture, Empty)
{
    EXPECT_FALSE(usual.isEmpty());
    EXPECT_TRUE (empty.isEmpty());
    EXPECT_FALSE(fully.isEmpty());
    EXPECT_FALSE(rect.isEmpty());
}
TEST_F(YoungTableauFixture, Full)
{
    EXPECT_FALSE(usual.isFull());
    EXPECT_FALSE(empty.isFull());
    EXPECT_FALSE(rect.isFull());
    EXPECT_TRUE (fully.isFull());
}
TEST_F(YoungTableauFixture, RowsIncreasing)
{
    for (int i = 0; i < usual.size().first; ++i)
        for (int j = 1; j < usual.size().second; ++j)
        {
            int elem = usual.get(i, j);
            int left = usual.get(i, j-1);
            ASSERT_GE(elem, left) << "Incorrect order in row " << i << "; column " << j;
        }

    for (int i = 0; i < fully.size().first; ++i)
        for (int j = 1; j < fully.size().second; ++j)
        {
            int elem = fully.get(i, j);
            int left = fully.get(i, j-1);
            ASSERT_GE(elem, left) << "Incorrect order in row " << i << "; column " << j;
        }

    for (int i = 0; i < rect.size().first; ++i)
        for (int j = 1; j < rect.size().second; ++j)
        {
            int elem = rect.get(i, j);
            int left = rect.get(i, j-1);
            ASSERT_GE(elem, left) << "Incorrect order in row " << i << "; column " << j;
        }
}
TEST_F(YoungTableauFixture, ColsIncreasing)
{
    for (int j = 0; j < usual.size().second; ++j)
        for (int i = 1; i < usual.size().first; ++i)
        {
            int elem = usual.get(i, j);
            int up = usual.get(i-1, j);
            ASSERT_GT(elem, up) << "Incorrect order in column " << j << "; row " << i;
        }

    for (int j = 0; j < fully.size().second; ++j)
        for (int i = 1; i < fully.size().first; ++i)
        {
            int elem = fully.get(i, j);
            int up = fully.get(i-1, j);
            ASSERT_GT(elem, up) << "Incorrect order in column " << j << "; row " << i;
        }

    for (int j = 0; j < rect.size().second; ++j)
        for (int i = 1; i < rect.size().first; ++i)
        {
            int elem = rect.get(i, j);
            int up = rect.get(i-1, j);
            ASSERT_GT(elem, up) << "Incorrect order in column " << j << "; row " << i;
        }
}
// fixme: check that insert method insert each item and only one time
// i.e. each item exists in end structure and there is no duplicates

TEST_F(YoungTableauFixture, SearchTest)
{
    for (int i = 1; i <= 16; ++i)
    {
        std::pair<int, int> result = empty.search(i);
        EXPECT_EQ(result.first, -1);
        EXPECT_EQ(result.second, -1);
    }
    for (int i = 1; i <= 16; ++i)
    {
        std::pair<int, int> result = fully.search(i);
        EXPECT_NE(result.first, -1);
        EXPECT_NE(result.second, -1);
    }
    for (int i = 1; i <= 12; ++i)
    {
        std::pair<int, int> result = usual.search(i);
        EXPECT_NE(result.first, -1);
        EXPECT_NE(result.second, -1);
    }
    for (int i = 1; i <= 6; ++i)
    {
        std::pair<int, int> result = rect.search(i);
        EXPECT_NE(result.first, -1);
        EXPECT_NE(result.second, -1);
    }
    std::pair<int, int> result = usual.search(14);

    EXPECT_EQ(result.first, 3);
    EXPECT_EQ(result.second, 0);
    result = usual.search(16);
    EXPECT_EQ(result.first, 3);
    EXPECT_EQ(result.second, 1);
    result = usual.search(13);
    EXPECT_EQ(result.first, -1);
    EXPECT_EQ(result.second, -1);
    result = usual.search(15);
    EXPECT_EQ(result.first, -1);
    EXPECT_EQ(result.second, -1);
    result = rect.search(8);
    EXPECT_EQ(result.first, -1);
    EXPECT_EQ(result.second, -1);
    result = rect.search(1);
    EXPECT_EQ(result.first, 0);
    EXPECT_EQ(result.second, 0);
    result = rect.search(5);
    EXPECT_EQ(result.first, 0);
    EXPECT_EQ(result.second, 2);
    result = rect.search(4);
    EXPECT_EQ(result.first, 1);
    EXPECT_EQ(result.second, 1);
}

