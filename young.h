#include <utility>
#include <algorithm>
#include <cmath>
#include <ostream>

#include <stdexcept>
#include <string>

#include <sstream>

class YoungTableauException: std::logic_error
{
  public:
    YoungTableauException(std::string msg) : std::logic_error(msg)
    {

    }
};

template <class T>
class infinitype
{
  private:
    T value;
    bool is_inf;

    static infinitype *infinity;
  public:
    infinitype(const T& val = 0) : value(val), is_inf(false) {}
    infinitype(const infinitype& o) : value(o.value), is_inf(o.is_inf) {}

    static infinitype& getInf()
    {
        if (!infinity)
        {
            infinity = new infinitype(1);
            infinity->is_inf = true;
        }
        return *infinity;
    }

    bool isFinite() const
    {
        return !is_inf;
    }

    bool isPositiveInf() const
    {
        return is_inf && value > 0;
    }
    bool isNegativeInf() const
    {
        return is_inf && value < 0;
    }

    infinitype operator-() const
    {
        infinitype copy(-this->value);
        copy.is_inf = this->is_inf;
        return copy;
    }

    bool operator>(const infinitype& o) const
    {
        return !o.isPositiveInf() && !this->isNegativeInf() &&
                (o.isNegativeInf() || this->isPositiveInf() || this->value > o.value);
        /*
        if(o.isPositiveInf())
            return false;
        if(o.isNegativeInf())
            return true;
        if(this->isPositiveInf())
            return true;
        if(this->isNegativeInf())
            return false;
        return this->value > o.value;
        */
    }
    bool operator<(const infinitype& o) const
    {
        return !this->isPositiveInf() && !o.isNegativeInf() &&
                (this->isNegativeInf() || o.isPositiveInf() || this->value < o.value);
        /*
        if(this->isPositiveInf())
            return false;
        if(this->isNegativeInf())
            return true;
        if(o.isPositiveInf())
            return true;
        if(o.isNegativeInf())
            return false;
        return this->value < o.value;
        */
    }
    bool operator==(const infinitype& o) const
    {
        return this->isFinite() && o.isFinite() && this->value == o.value;
    }

    const T& get() const
    {
        if (!is_inf)
            return this->value;
        // fixme: correct infinity value
        if (this->value >= 0)
            return INFINITY;
        return -INFINITY;
    }
    void set(const T& val)
    {
        value = val;
        is_inf = false;
    }
};
template <class T>
infinitype<T>* infinitype<T>::infinity = NULL;

template <class T>
std::ostream& operator<<(std::ostream& o, infinitype<T>& t)
{
    if (t.isNegativeInf())
        o << "-inf";
    else if (t.isPositiveInf())
        o << "inf";
    else
        o << t.get();
}

template <class T>
class YoungTableau
{
    infinitype<T> ** inner_tableau;
    int m;
    int n;

  public:
    YoungTableau(int M, int N):m(M), n(N)
    {
        inner_tableau = new infinitype<T>* [M];
        for (int i = 0; i < M; ++i)
        {
            inner_tableau[i] = new infinitype<T> [N];
            std::fill_n(inner_tableau[i], N, infinitype<T>::getInf());
        }
    }
    ~YoungTableau()
    {
        for (int i = 0; i < m; ++i)
        {
            delete [] inner_tableau[i];
        }
        delete [] inner_tableau;
    }

    bool isEmpty()
    {
        return !inner_tableau[0][0].isFinite();
    }
    bool isFull()
    {
        return inner_tableau[m-1][n-1].isFinite();
    }

    void insert(const T value)
    {
        if (isFull())
            throw YoungTableauException("tableau is full");
        // look for a non full row
        int i = 0, j= 0;
        for (; i < m; ++i)
            if (inner_tableau[i][n-1].isPositiveInf())
                break;
        for (; j < n; ++j)
            if (inner_tableau[i][j].isPositiveInf())
            {
                inner_tableau[i][j].set(value);
                break;
            }
        // move up
        infinitype<T> up_neighbour = i > 0 ? inner_tableau[i-1][j] : -infinitype<T>::getInf();
        infinitype<T> left_neighbour = j > 0 ? inner_tableau[i][j-1] : -infinitype<T>::getInf();
        while (!(up_neighbour < value && left_neighbour < value))
        {
            // check for bounds
            infinitype<T> max_neighbour = std::max(up_neighbour, left_neighbour);
            if (max_neighbour == up_neighbour)
            {
                inner_tableau[i-1][j].set(value);
                inner_tableau[i][j].set(up_neighbour.get());
                i = i-1;
            }
            else if (max_neighbour == left_neighbour)
            {
                inner_tableau[i][j-1].set(value);
                inner_tableau[i][j].set(left_neighbour.get());
                j = j-1;
            }
            up_neighbour = i > 0 ? inner_tableau[i-1][j] : -infinitype<T>::getInf();
            left_neighbour = j > 0 ? inner_tableau[i][j-1] : -infinitype<T>::getInf();
        }
    }

    /// TODO
    std::pair<int, int> search(const T value) const
    {
        // look for last
        int i = m-1, j= 0;
        while (i >= 0 && j <= n)
        {
            if (inner_tableau[i][j].get() > value)
            {
                i -= 1;
                continue;
            }
            if (inner_tableau[i][j].get() < value)
            {
                j += 1;
                continue;
            }
            if (inner_tableau[i][j].get() == value)
                return std::pair<int, int>(i, j);
        }
        return std::pair<int, int>(-1, -1);
    }

    T extract_min();

    std::string getPrintableTableau() const
    {
        std::stringstream outstr;
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
                outstr << inner_tableau[i][j] << " ";
            outstr << std::endl;
        }
        return outstr.str();
    }

    std::pair<int, int> size() const
    {
        return std::pair<int, int>(m, n);
    }
    T get(int i, int j) const
    {
        return inner_tableau[i][j].get();
    }
};
